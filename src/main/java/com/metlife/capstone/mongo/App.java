package com.metlife.capstone.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	MongoClient mongoClient;
        //System.out.println( "Hello World!" );
    	try {
    		
			mongoClient = new MongoClient("localhost" , 27017);
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
    	
    	DB db = mongoClient.getDB( "test" );
    	
    	Set<String> colls = db.getCollectionNames();
    	
    	for (String s: colls) {
			System.out.println(s);
		}
    	
    	
    	
    }
}
